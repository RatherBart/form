$(function() {

 		$('#name_error').hide();
        $('#last_name_error').hide();
        $('#email_error').hide();
        $('#phone_error').hide();
        $('#street_error').hide();
        $('#home_number_error').hide();
        $('#zip_code_error').hide();
        $('#city_error').hide();

    function init() {
        var regex = {
            name: /^([a-zA-Z]){5,20}$/,
            last_name:/^([a-zA-Z]){5,20}$/,
            email: /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/,
            phone: /^(\d{3})\-?(\d{3})\-?(\d{3})/,
            street: /^[A-Z]+[a-z]*$/,
            home_number: /^[0-9]{1,}$/,
            zip_code: /^([0-9]){2}\-[0-9]{3}$/,
            city:/^[A-Za-z]{0,20}$/,
        };
       


        $.each($('input:not([type="submit"])'), function(){
            $(this).on('focusout',function(){
                if(!regex[$(this).attr('name')].test($(this).val())){
                    $(this).css('border-color','red');
                  	$(this).next().css('color', 'red').show();
                } else {
                    $(this).css('border-color','#ced4da'); 
                    $(this).next().hide();
                    $('div button#contact').click(onFormButtonClicked);

                }
            });
        });

        $('#contact_phone').keyup(function(){
   		 	$(this).val($(this).val().replace(/(\d{3})\-?(\d{3})\-?(\d{3})/,'$1-$2-$3'))
		});
		$('#contact_zip_code').keyup(function(){
   		 	$(this).val($(this).val().replace(/(\d{2})\-?(\d{3})/,'$1-$2'))
		});
    }


    getFormData = function() {
        var fd = {
            name: $('#contact_name').val().trim(),
            last_name:$("#contact_lastname").val().trim(),
            email: $('#contact_email').val().trim(),
            phone: $('#contact_phone').val().trim(),
            street: $('#contact_street').val().trim(),
            home_number: $('#contact_home_number').val().trim(),
            zip_code: $('#contact_zip_code').val().trim(),
            city: $('#contact_city').val().trim()
        };
       
        if (fd.name.length == 0 || fd.last_name.length == 0 || fd.email.length == 0 || fd.phone.length == 0) {
            return {
                error: true
            };
        }

        return fd;
    };
   
    cleanFormData = function() {
        $('#contact_name').val('');
        $("#contact_lastname").val('');
        $('#contact_email').val('');
        $('#contact_phone').val('');
        $('#contact_street').val('');
        $('#contact_home_number').val('');
        $('#contact_zip_code').val('');
        $('#contact_city').val('')

        $.each($('input:not([type="submit"])'), function(){
            $(this).css('border-color','#ced4da'); 
            $(this).next().hide();
        });

    };


    onFormButtonClicked = function() {
        var sel = $('#contact-sending-status');
        sel.removeClass('sending-failed');
        sel.removeClass('sending-ok');
        var fd = getFormData();
        if (fd.error == true) {
            sel.addClass('sending-failed');
            sel.html('Please, fill Last Name, Name, Email and Phone fields ');
            return;
        }
        /*Data prepared for catch in php*/
        $.post('send.php', fd, function() {
            sel.addClass('sending-ok');
            sel.html('Thank you, message has been sent');
            cleanFormData();
        }).fail(function() {
            sel.addClass('sending-failed');
            sel.html('Sending error, please try later');
        });
    };

    init();
});